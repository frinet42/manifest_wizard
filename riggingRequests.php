<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<!-- href="appointment.css" -->
	<head>
		<link type="text/css" rel="stylesheet" href= "Loads.css" ></link> 
		<title>Current Loads</title>
	</head>
	<body link="white">	
<?php
	@ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

	
	$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
    // Check if there were error and if so, report and exit

    if (mysqli_connect_errno()) 
    { 
      echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
      exit;
    }

  
	$numRequests = "SELECT  COUNT(*) FROM Rigging";
	
	// run the SQL query to retrieve the service partner info
	$totalRequests = $db->query($numRequests);
	$numRequests = $totalRequests->fetch_assoc();
	// determine how many rows were returned
    $num_results = $numRequests['COUNT(*)'];
	if($num_results == 0){
		print '<tr><td> No requests yet. </td></tr>';
	}else{
    // loop through each row building the table rows and data columns
	
	
    for ($i=1; $i <= $num_results; $i++) 
    {
		$numQuery = $db->query("SELECT  COUNT(*) FROM Rigging WHERE (Rigging_SerialNum = {$i})");
		$Requests = $numQuery->fetch_assoc();
		if($Requests['COUNT(*)'] == 1){
		$numQuery = $db->query("SELECT  * FROM Rigging WHERE (Rigging_SerialNum = {$i})");
		$Requests = $numQuery->fetch_assoc();
		$uspanum = $Requests['USPA_Num'];
		$name = $db->query("SELECT  * FROM Skydiver WHERE (USPA_Num = {$uspanum})");
		$name = $name->fetch_assoc();
		print '<div> <table> <caption>Request Number: '.$i.'</caption>';
		print '<tr><td> USPA #: </td><td>'.$Requests['USPA_Num'].'</td></tr>';
		print '<tr><td> First Name: </td><td>'.$name['Skydiver_FName'].'</td></tr>';
		print '<tr><td> Last Name: </td><td>'.$name['Skydiver_LName'].'</td></tr>';
		print '<tr><td> Request Type: </td><td>'.$Requests['Rigging_WorkDone'].'</td></tr>';
		print '<tr><td> Container Serial #: </td><td>'.$Requests['Rigging_Container'].'</td></tr>';
		print '<tr><td> Main: </td><td>'.$Requests['Rigging_Main'].'</td></tr>';
		print '<tr><td> Reserve: </td><td>'.$Requests['Rigging_Reserve'].'</td></tr>';
		print '<tr><td> AAD: </td><td>'.$Requests['Rigging_AAD'].'</td></tr>';
		}else{
			$num_results = $num_results+1;
		}
	}
	print '</table></div>';
	}
		?>
	<footer>
			<a href = "HomePage.html">Home</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Login.html">Login</a>
			<a id = "space"> &nbsp  </a>
			<a href = "CurrentLoads.php">View Current Loads</a>
			<a id = "space"> &nbsp  </a>
			<a href = "ViewInformation.html">View Skydiver Information</a>
			<a id = "space"> &nbsp  </a>
			<a href = "AdminLogin.php">Administrator</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Rigging.html">Rigging</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Logout.php">Logout</a>
	</footer>
	</body>
</html>