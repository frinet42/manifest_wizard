<html>
 <head>
   <title>Update Skydiver Confirmation Page</title>
   <style> 
     body {font-family: Arial; color: black; } 
     h1 {background-color: blue; color: white; }
   </style>
 </head>

 <body>
   <center>
     <h1>Skydiver Updated!</h1>
     <br />
     <br />

<!-- Note the use of <?php ?> to embed PHP commands 
     and $_POST['<parameter_name>'] to get POST parameters -->

     <?php 

     // open connection to the database on LOCALHOST with 
     // userid of 'root', password 'secret', and database 'csl'

     @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

     // Check if there were error and if so, report and exit

     if (mysqli_connect_errno()) 
     { 
       echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
       exit;
     }


     // sanitize the input from the form to eliminate possible SQL Injection
	
	$USPA_Num = stripslashes($_POST['USPANum']);
	$USPA_Num = htmlspecialchars($USPA_Num);
    $USPA_Num = $db->real_escape_string($USPA_Num);
	
	$request = stripslashes($_POST['updateInfo']);
	$request = htmlspecialchars($request);
    $request = $db->real_escape_string($request);
	
	$newInfo = stripslashes($_POST['newInfo']);
	$newInfo = htmlspecialchars($newInfo);
    $newInfo = $db->real_escape_string($newInfo);
    
				//Server side data validation
				$validUSPANum = "/^\d{6}$/";
				$validPassword = "/^\S+$/";
				$validPasswordShort = "/^\S{1,5}$/";
				$validPasswordLong = "/^\S{16,}$/";
				$validBlanketDate = "/^\d{4}\-\d{2}\-\d{2}$/";
				$validNameOrType = "/^[a-zA-z]+$/";
				$validLicense = "/^[ABCD]\-\d{1,}$/";
				$validJumpNum = "/^\d{1,}$/";
				
				if(!preg_match($validUSPANum,$USPA_Num)){
					echo("INSERT ERROR: Please enter your valid USPA number");
					header("Location:Administrator.php");
				}
				if($request == "Skydiver_FName" || $request == "Skydiver_LName" ){
					if(!preg_match($validNameOrType,$newInfo)){
					echo("INSERT ERROR: Please enter a valid name");
					header("Location:Administrator.php");
				}
				}
				
				if($request == "Liscence_Num"){
					if(!preg_match($validLicense,$newInfo)){
					echo("INSERT ERROR: Please enter a valid license number (L-DDDD...)");
					header("Location:Administrator.php");
				}
				}
				
				if($request == "Skydiver_DateBeganSport" || $request == "Skydiver_DOB" || $request =="Skydiver_ReserveRepackDate"
				|| $request == "Skydiver_DateOfLastJump"){
					if(!preg_match($validBlanketDate,$newInfo)){
					echo("INSERT ERROR: Please enter a valid date (YYYY-MM-DD)");
					header("Location:Administrator.php");
				}
				}
				
				if($request == "Skydiver_Type"){
					if(!preg_match($validNameOrType,$newInfo)){
					echo("INSERT ERROR: Please enter a valid skydiver type.");
					header("Location:Administrator.php");
				}
				}
				if($request == "Skydiver_NumOfJumps"){
				if(!preg_match($validJumpNum,$newInfo)){
					echo("INSERT ERROR: Please enter a valid jump number");
					header("Location:Administrator.php");
				}
				}
				if($request == "Skydiver_Password"){
				if(!preg_match($validPassword,$newInfo)){
					echo("INSERT ERROR: Please enter your password.");
					header("Location:Administrator.php");
				}
				
				if(preg_match($validPasswordShort,$newInfo)){
					echo("INSERT ERROR: The password entered was too short.");
					header("Location:Administrator.php");
				}
				if(preg_match($validPasswordLong,$newInfo)){
					echo("INSERT ERROR: The password entered was too long.");
					header("Location:Administrator.php");
					
				}
				}
	
	
	if($_POST['updateInfo'] == 'Skydiver_Password'){
		$newInfo = md5($newInfo);
	}
	if($_POST['updateInfo'] == 'Skydiver_Admin'){
		$newInfo = 'Y';
	}

	// set up a prepared statement to insert the Skydiver
	 /*
	-- -----------------------------------------------------
	-- Table `Manifest_Wizard`.`Skydiver`
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS `Manifest_Wizard`.`Skydiver` ;

	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Skydiver` (
	`USPA_Num` INT NOT NULL,
	`Liscence_Num` INT NULL,
	`Skydiver_DateBeganSport` DATE NOT NULL,
	`Skydiver_NumOfJumps` INT NOT NULL,
	`Skydiver_FName` CHAR(45) NOT NULL,
	`Skydiver_LName` CHAR(45) NOT NULL,
	`Skydiver_DOB` DATE NOT NULL,
	`Skydiver_ReserveRepackDate` DATE NULL,
	`Skydiver_DateOfLastJump` DATE NULL,
	`Skydiver_Type` CHAR(1) NOT NULL,
	`Skydiver_Password` VARCHAR(8),
	'Skydiver_Admin' BOOLEAN,
	PRIMARY KEY (`USPA_Num`))
	ENGINE = InnoDB;

	*/

  $query = $db->query("UPDATE SKYDIVER SET {$request}='{$newInfo}' WHERE (USPA_Num = {$USPA_Num})");

     echo 'Information updated.<br/><br/>';  // report results
	 
     $db->close();    // close the database connection
	 
	 header("location:Administrator.php");
   ?>

   </center> 
 </body>
</html> 