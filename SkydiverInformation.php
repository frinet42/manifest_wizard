<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<!-- href="appointment.css" -->
	<head>
		<link type="text/css" rel="stylesheet" href= "Loads.css" ></link> 
		<title>Current Loads</title>
	</head>
	<body link="white">
	
	
	<div>
		<table>
			<caption>Skydiver Information</caption>
	<?php
	if(!isset($_POST['USPAnumber'])){
	header("location:ViewInformation.html");
	exit;
	}

	@ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

	
	$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
    // Check if there were error and if so, report and exit

    if (mysqli_connect_errno()) 
    { 
      echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
      exit;
    }

    // sanitize the input from the form to eliminate possible SQL Injection

	/*
	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Skydiver` (
	`USPA_Num` INT NOT NULL,
	`Liscence_Num` INT NULL,
	`Skydiver_DateBeganSport` DATE NOT NULL,
	`Skydiver_NumOfJumps` INT NOT NULL,
	`Skydiver_FName` CHAR(45) NOT NULL,
	`Skydiver_LName` CHAR(45) NOT NULL,
	`Skydiver_DOB` DATE NOT NULL,
	`Skydiver_ReserveRepackDate` DATE NULL,
	`Skydiver_DateOfLastJump` DATE NULL,
	`Skydiver_Type` CHAR(1) NOT NULL,
	`Skydiver_Password` VARCHAR(8),
	PRIMARY KEY (`USPA_Num`))
	ENGINE = InnoDB;


	*/
    $USPA_Num = stripslashes($_POST['USPAnumber']);
    $USPA_Num = $db->real_escape_string($USPA_Num);
	
	//Server side Data Validation
	
	$validUSPANum = "/^\d{6}$/";

	if(!preg_match($validUSPANum,$USPA_Num)){
					echo("INSERT ERROR: Please enter your valid USPA number");
					header("Location:Administrator.php");
				}
				
				
	$Information = "SELECT * FROM SKYDIVER WHERE(USPA_Num = {$USPA_Num})";
	
	// run the SQL query to retrieve the service partner info
	$Skydiver_Info = $db->query($Information);
	
	// determine how many rows were returned
    $num_results = $Skydiver_Info->num_rows;
	if($num_results == 0){
		print '<tr><td> Skydiver does not exist. </td></tr>';
	}else{
    // loop through each row building the table rows and data columns
	
    for ($i=0; $i < $num_results; $i++) 
    {
    $r= $Skydiver_Info->fetch_assoc();
	print '<tr><td> First Name: </td><td>'.$r['Skydiver_FName'].'</td></tr>';
	print '<tr><td> Last Name: </td><td>'.$r['Skydiver_LName'].'</td></tr>';
	print '<tr><td> License #: </td><td>'.$r['Liscence_Num'].'</td></tr>';
	print '<tr><td> Time in Sport: </td><td>'.$r['Skydiver_DateBeganSport'].'</td></tr>';
	print '<tr><td> # of Jumps: </td><td>'.$r['Skydiver_NumOfJumps'].'</td></tr>';
	print '<tr><td> Reserve Repack: </td><td>'.$r['Skydiver_ReserveRepackDate'].'</td></tr>';
	print '<tr><td> Last Jump: </td><td>'.$r['Skydiver_DateOfLastJump'].'</td></tr>';
	print '<tr><td> Type: </td><td>'.$r['Skydiver_Type'].'</td></tr>';
	}
	}
	
	
	
	
	
	?>
		<form name = "View_All_Skydivers" action = "AllSkydivers.php" method = "POST">
		<tr> 
			<td colspan = "2">
			<input type="submit" value = "View All Skydivers">
			</td>
		</tr>
		</table>
		</div>
		
	<footer>
			<a href = "HomePage.html">Home</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Login.html">Login</a>
			<a id = "space"> &nbsp  </a>
			<a href = "CurrentLoads.php">View Current Loads</a>
			<a id = "space"> &nbsp  </a>
			<a href = "ViewInformation.html">View Skydiver Information</a>
			<a id = "space"> &nbsp  </a>
			<a href = "AdminLogin.php">Administrator</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Rigging.html">Rigging</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Logout.php">Logout</a>
	</footer>
	</body>
</html>