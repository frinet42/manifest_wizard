<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<!-- href="appointment.css" -->
	<head>
		<link type="text/css" rel="stylesheet" href= "Loads.css" ></link> 
		<title>Load Printer</title>
	</head>
	<body link="white">	
<?php
	@ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

	
	$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
    // Check if there were error and if so, report and exit

    if (mysqli_connect_errno()) 
    { 
      echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
      exit;
    }

  
	$numLoad = stripslashes($_POST['loadNum']);
	$numLoad = htmlspecialchars($numLoad);
    $numLoad = $db->real_escape_string($numLoad);
	
	//Server side data validation
	$validLoadNumber = "/^\d{1,}$/";
				
	if(!preg_match($validLoadNumber,$numLoad)){
			echo("LOAD ERROR: The selected load is not valid.");
			header("Location:Administrator.php");
	}

  
	$numQuery = $db->query("SELECT  Load_Capacity, Load_Time FROM _Load WHERE (Load_Num = {$numLoad})");
	$loadTime = $numQuery->fetch_assoc();
	$setTime = $loadTime['Load_Time'];
	$times = explode(":", $setTime);
	print '<div> <table> <caption>Load Number: '.$numLoad.' @ '.$times[0].':'.$times[1].'</caption>';
	$Skydiver = $db->query("SELECT  * FROM Load_Assignment WHERE Load_Num = {$numLoad} ORDER BY Jump_Type ASC, Group_Size DESC");
	print '<tr><th> Pax #</th><th> Name (group)</th></tr>';
	$j = 1;
	while ($r = $Skydiver->fetch_assoc())
	{	
		print '<tr><td>'.$j.'</td><td>'.$r['_Name'].' ('.$r['Group_Name'].')'.'</td></tr>';
		$j = $j + 1;
	}
	while ($j <= $loadTime['Load_Capacity']){
			print '<tr><td>'.$j.'</td><td></td></tr>';
			$j = $j + 1;
	}
	print '</table></div>';

?>
<footer>
		<a href = "HomePage.html">Home</a>
		<a id = "space"> &nbsp  </a>
		<a href = "Login.html">Login</a>
		<a id = "space"> &nbsp  </a>
		<a href = "CurrentLoads.php">View Current Loads</a>
		<a id = "space"> &nbsp  </a>
		<a href = "ViewInformation.html">View Skydiver Information</a>
		<a id = "space"> &nbsp  </a>
		<a href = "AdminLogin.php">Administrator</a>
		<a id = "space"> &nbsp  </a>
		<a href = "Rigging.html">Rigging</a>
		<a id = "space"> &nbsp  </a>
		<a href = "Logout.php">Logout</a>
</footer>
</body>
</html>