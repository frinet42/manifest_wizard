<?
	<?PHP
 session_start();
 if ( (isset($_SESSION['Username']) || isset($_SESSION['Admin'])) )
  {
	header("location:Administrator.php");
	exit;
  }

?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<!-- href="appointment.css" -->
	<head>
		<link type="text/css" rel="stylesheet" href= "Manifest.css" ></link> 
		<title>Admin Login Page</title>
		<!--<script type="text/javascript">
			
			var validName= /^\w{1,}+\s+\w{1}+[.]+\s+\w{1,}/;
			var validSocial= /^\d{3}+-+\d{2}+-+\d{4}$/;
			var validDate= /^\d{2}+[\/]+\d{2}+[\/]+\d{4}/;
			function properFormat()
			{
				var format = new Boolean(false);
				
				var Name = document.forms["newAppointment"]["patientName"].value;
				
				if(!validName.test(patientName)){
					format = false;
					alert("Please enter your name in format (First M. Last).");
				}
				
				var Social = document.forms["newAppointment"]["SSN"].value;
				
				if(!validSocial.test(SSN)){
					format = false;
					alert("Please enter your Social Security number in (XXX-XX-XXXX) format.");
				}
				
				var Date = document.forms["newAppointment"]["date"].value;
				
				if(!validDate.test(date)){
					format = false;
					alert("Please enter the appointment date in (DD/MM/YYYY) format.");
				}
				return format;
			}
		</script>
		-->
	</head>
	<body link="white">
	<!-- action="Demo.php" onsubmit="return properFormat();"-->
	<form name = "LoginInformation" action="checkAdminLogin.php" method = "POST">
	<table align = "center">
	<tr>
	<th colspan = "2"> Login</th>
	</tr>
		<tr>
			<td>
			Administrator Username:
			</td>
			<td>
			<input type="text" name="Username" placeholder = "123456">
			</td>
		</tr>
		<tr>
			<td>
			Password:
			</td>
			<td>
			<input type="text" name="Password" placeholder = "12345678">
			</td>
		</tr>
			<!-- We need to hash this -->
		<tr> 
			<td colspan = "2">
			<input type="submit"value = "Login">
			</td>
		</tr>
	</table>
	</form>
	<footer>
		<div> 
			<a href = "HomePage.html">Home</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Login.html">Login</a>
			<a id = "space"> &nbsp  </a>
			<a href = "CurrentLoads.php">View Current Loads</a>
			<a id = "space"> &nbsp  </a>
			<a href = "ViewInformation.html">View Skydiver Information</a>
			<a id = "space"> &nbsp  </a>
			<a href = "AdminLogin.php">Administrator</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Rigging.html">Rigging</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Logout.php">Logout</a>
		</div>
	</footer>
	</body>
</html>