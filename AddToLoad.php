<html>
 <head>
   <title>Add Skydiver to Load Confirmation Page</title>
   <style> 
     body {font-family: Arial; color: black; } 
     h1 {background-color: blue; color: white; }
   </style>
 </head>

 <body>
   <center>
     <h1>You're on the load!</h1>
     <br />
     <br />

<!-- Note the use of <?php ?> to embed PHP commands 
     and $_POST['<parameter_name>'] to get POST parameters -->

     <?php 
	session_start();
	if ( !isset($_SESSION['Username']) || !isset($_SESSION['Name']) ) 
	{
	header("location:Login.html");
	exit;
	}
     // open connection to the database on LOCALHOST with 
     // userid of 'root', password 'secret', and database 'csl'

     @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

     // Check if there were error and if so, report and exit

     if (mysqli_connect_errno()) 
     { 
       echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
       exit;
     }
	
     // sanitize the input from the form to eliminate possible SQL Injection
	$USPA_Num = $_SESSION['Username'];
    $USPA_Num = $db->real_escape_string($USPA_Num);
	
	$_Name = $_SESSION['Name'];
    $_Name = $db->real_escape_string($_Name);
	
	$Load_Num = stripslashes($_POST['LoadNumber']);
	$Load_Num = htmlspecialchars($Load_Num);
    $Load_Num = $db->real_escape_string($Load_Num);
	
	$Jump_Type = stripslashes($_POST['jumpType']);
	$Jump_Type = htmlspecialchars($Jump_Type);
    $Jump_Type = $db->real_escape_string($Jump_Type);

	$Group_Name = stripslashes($_POST['GroupName']);
	$Group_Name = htmlspecialchars($Group_Name);
	$Group_Name = $db->real_escape_string($Group_Name);

	// Server side data validation
	 
	$validUSPANum = "/^\d{6}$/";
	$validLoadNumber = "/^\d{1,}$/";
	$validJump_Type = "/^[0-8]$/";
	$validGroup_Name = "/^\S{1,20}$/";
	
	if(!preg_match($validUSPANum,$USPA_Num)){
					echo("INSERT ERROR: Please enter your valid USPA number");
					header("Location:CurrentLoads.php");
				}
	if(!preg_match($validLoadNumber,$Load_Num)){
					echo("INSERT ERROR: Please enter a valid Load Number");
					header("Location:CurrentLoads.php");
				}
	if(!preg_match($validJump_Type,$Jump_Type)){
					echo("INSERT ERROR: Please enter a valid Jump Type");
					header("Location:CurrentLoads.php");
				}
	if(!preg_match($validGroup_Name,$Group_Name)){
					echo("INSERT ERROR: Please select a group name");
					header("Location:CurrentLoads.php");
				}
	
     // set up a prepared statement to insert the Skydiver
	 /*
	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Load_Assignment` (
	`Load_Num` INT NOT NULL,
	`USPA_Num` INT NOT NULL,
	`_Name` VARCHAR(25) NOT NULL,
	`Jump_Type` VARCHAR(15) NOT NULL,
	`Group_Name` VARCHAR(20) NOT NULL,
	`Jump_Order` INT NOT NULL,
	PRIMARY KEY (`Load_Num`, `USPA_Num`),
	INDEX `USPA_Num_idx` (`_Name` ASC),
	CONSTRAINT `Load_Assignment_Load_Num`
		FOREIGN KEY (`Load_Num`)
		REFERENCES `Manifest_Wizard`.`Load` (`Load_Num`)
		ON DELETE RESTRICT
		ON UPDATE RESTRICT,
	CONSTRAINT `Load_Assignment_USPA_Num`
		FOREIGN KEY (`USPA_Num`)
		REFERENCES `Manifest_Wizard`.`Skydiver` (`USPA_Num`)
		ON DELETE RESTRICT
		ON UPDATE RESTRICT)
	ENGINE = InnoDB;


	*/
	$Count = $db->query("SELECT COUNT(*) FROM Load_Assignment WHERE Load_Num = {$Load_Num}");
	$c = $Count->fetch_assoc();
	var_dump($c['COUNT(*)']);
	$Capacity = $db->query("SELECT * FROM _Load WHERE Load_Num = {$Load_Num}");
	$cap = $Capacity->fetch_assoc();
	var_dump($cap['Load_Capacity']);
	if($c['COUNT(*)'] < $cap['Load_Capacity'] ){
     $query = "INSERT INTO Load_Assignment (Load_Num, USPA_Num, _Name, Jump_Type, Group_Name, Group_Size) 
	 VALUES ( ?, ?, ?, ?, ?,?)";  // question marks are parameter locations
	
     $stmt = $db->prepare($query);  // creates the Prepared Statement
	
	 // determine how many rows were returned
	$Group_Size = 1;
	 // binds the parameters of Prepared Statement to corresponding variables
	 // first argument, "sssiss", gives the parameter data types of 3 strings, an int, 2 strings
     $stmt->bind_param("iisisi", $Load_Num, $USPA_Num, $_Name, $Jump_Type, $Group_Name,$Group_Size);

     $stmt->execute();  // runs the Prepared Statement query
	
     //echo $stmt.' records inserted.<br/><br/>';  // report results
     $stmt->close();  // deallocate the Prepared Statement
	 $db->close();    // close the database connection
	}else{
		echo "<script type='text/javascript'>alert('The load is full, please chose a different load');</script>";
	}
	 header("location:CurrentLoads.php");
	 exit;
	 ?>

   </center> 
 </body>
</html> 