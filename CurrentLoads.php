<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<!-- href="appointment.css" -->
	<head>
		<link type="text/css" rel="stylesheet" href= "Loads.css" ></link> 
		<title>Current Loads</title>
	</head>
	<body link="white">	
	
			<script type="text/javascript">
			var validGroupName = /^\S{1,20}$/;
			var validLoadNumber = /^\d{1,}$/;
			function properFormat()
			{
				var format = new Boolean(true);
				
				var groupName = document.forms["AddToLoad"]["GroupName"].value;
				var loadNumber = document.forms["AddToLoad"]["LoadNumber"].value;
				
				if(!validLoadNumber.test(loadNumber)){
					format = false;
					alert("INFORMATION ERROR: Please enter a valid load number.");
				}
				if(!validGroupName.test(groupName)){
					format = false;
					alert("INFORMATION ERROR: Please enter a valid Group Name between 1 and 20 characters.");
				}
				return format;
			}
		</script>
<?php
	@ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
	date_default_timezone_set('America/Denver');
	
	$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
    // Check if there were error and if so, report and exit

    if (mysqli_connect_errno()) 
    { 
      echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
      exit;
    }

  
	$numLoads = "SELECT  COUNT(*) FROM _Load";
	
	// run the SQL query to retrieve the service partner info
	$totalLoads = $db->query($numLoads);
	$numLoads = $totalLoads->fetch_assoc();
	// determine how many rows were returned
    $num_results = $numLoads['COUNT(*)'];
	if($num_results == 0){
		print '<tr><td> No loads yet. </td></tr>';
	}else{
    // loop through each row building the table rows and data columns
	
	
    for ($i=1; $i <= $num_results; $i++) 
    {
		$loadExistQuery = $db->query("SELECT COUNT(*) FROM _Load WHERE (Load_Num = {$i})");
		$rows = $loadExistQuery->fetch_assoc();
		if($rows['COUNT(*)'] == 1){
		$numQuery = $db->query("SELECT  Load_Capacity, Load_Time FROM _Load WHERE (Load_Num = {$i})");
		$numSpots = $numQuery->fetch_assoc();
		// Time function
		$setTime = $numSpots['Load_Time'];
		$times = explode(":", $setTime);
		$today = getdate();
		$date = mktime(0, 0, 0, date($today['mon'])  , date($today['mday']), date($today['year']));
		$unixTime = time();
		
		sscanf($numSpots['Load_Time'], "%d:%d:%d", $hours, $minutes, $seconds);
		$sTime = $hours*3600 + $minutes*60 + $seconds;
		$loadTime = $date + $sTime;
		$difference =  $loadTime-$unixTime;
		$init = $difference;
		$hours = floor($init / 3600);
		$minutes = floor(($init / 60) % 60);
		$seconds = $init % 60;
		
		if($hours == 0 && $minutes == 0){
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' (Complete) </caption>';
		}else if ($hours == 0 && $minutes == 1){
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' ('.$minutes.' minute) </caption>';
		}else if($hours == 0 && $minutes > 1){
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' ('.$minutes.' minutes) </caption>';
		}else if ($hours == 1 && $minutes == 1){
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' ('.$hours.' hour '.$minutes.' minute) </caption>';
		}else if($hours == 1){
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' ('.$hours.' hour '.$minutes.' minutes) </caption>';
		}else if($hours > 1 && $minutes == 1){
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' ('.$hours.' hours '.$minutes.' minute) </caption>';
		}else if($hours > 1){
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' ('.$hours.' hours '.$minutes.' minutes) </caption>';
		}else{
			print '<div> <table> <caption>Load Number: '.$i.' @ '.$times[0].':'.$times[1].' (Complete) </caption>';
		}
		print '<tr><th> Pax #</th><th> Name (group)</th></tr>';
		$Skydiver = $db->query("SELECT  * FROM Load_Assignment WHERE Load_Num = {$i}");
		while ($r = $Skydiver->fetch_assoc())
		{	
			$GroupName = $r['Group_Name'];
			$Count = $db->query("SELECT COUNT(*) FROM Load_Assignment WHERE Load_Num = {$i} AND Group_Name = '{$GroupName}'");
			$c = $Count->fetch_assoc();
			$USPA = $r['USPA_Num'];
			$updateJumpDate = $db->query("UPDATE Load_Assignment SET Group_Size = {$c['COUNT(*)']} WHERE (Load_Num = {$i} AND Group_Name = '{$GroupName}' AND USPA_Num = {$USPA})");
		}
		
		$Skydiver = $db->query("SELECT  * FROM Load_Assignment WHERE Load_Num = {$i} ORDER BY Jump_Type DESC, Group_Size ASC");
		$j = 1;
		while ($r = $Skydiver->fetch_assoc() )
		{
			if($j <= $numSpots['Load_Capacity']){
			print '<tr><td>'.$j.'</td><td>'.$r['_Name'].' ('.$r['Group_Name'].')'.'</td></tr>';
			$j = $j + 1;
			}
		}
		while ($j <= $numSpots['Load_Capacity']){
			print '<tr><td>'.$j.'</td><td></td></tr>';
			$j = $j + 1;
		}
		print '</table></div>';
		}else{
			$num_results = $num_results+1;
		}
		/*
		$r= $Skydiver_Info->fetch_assoc();
	
	print '<div><table><caption>Skydiver Information</caption>';
	print '<tr><td> First Name: </td><td>'.$r['Skydiver_FName'].'</td></tr>';
	print '<tr><td> Last Name: </td><td>'.$r['Skydiver_LName'].'</td></tr>';
	print '<tr><td> License #: </td><td>'.$r['Liscence_Num'].'</td></tr>';
	print '<tr><td> Time in Sport: </td><td>'.$r['Skydiver_DateBeganSport'].'</td></tr>';
	print '<tr><td> # of Jumps: </td><td>'.$r['Skydiver_NumOfJumps'].'</td></tr>';
	print '<tr><td> Date of Birth: </td><td>'.$r['Skydiver_DOB'].'</td></tr>';
	print '<tr><td> Reserve Repack: </td><td>'.$r['Skydiver_ReserveRepackDate'].'</td></tr>';
	print '<tr><td> Last Jump: </td><td>'.$r['Skydiver_DateOfLastJump'].'</td></tr>';
	print '<tr><td> Type: </td><td>'.$r['Skydiver_Type'].'</td></tr></table></div>';
    */
	}
	}
		?>
		
			<!-- action="Demo.php" onsubmit="return properFormat();"-->
		<form name = "AddToLoad" action = "AddToLoad.php" onsubmit="return properFormat();" method = "POST" align = "right">
		<table align = "center">
			<tr>
			<th colspan = "2">Add me to a load:</th>
			</tr>
			<tr>
			<td>Load Number: </td>
			<td><select name = "LoadNumber">
			<?php
				$numLoads = "SELECT  COUNT(*) FROM _Load";
				// run the SQL query to retrieve the service partner info
				$totalLoads = $db->query($numLoads);
				$numLoads = $totalLoads->fetch_assoc();
				// determine how many rows were returned
				$num_results = $numLoads['COUNT(*)'];
				
				 for ($i=1; $i <= $num_results; $i++) 
				{
				$loadExistQuery = $db->query("SELECT COUNT(*) FROM _Load WHERE (Load_Num = {$i})");
				$rows = $loadExistQuery->fetch_assoc();
				if($rows['COUNT(*)'] == 1){
					print '<option value="'.$i.'">'.$i.'</option>';
				}else{
					$num_results = $num_results+1;
				}
				}
			?>
			</select></td>
			</tr>
			<tr>
			<td>Type of Jump:</td>
			<td><input type="radio" name="jumpType" value="0" checked>Hop n Pop
			<br>
			<input type="radio" name="jumpType" value="1">Relative Work
			<br>
			<input type="radio" name="jumpType" value="2">Freeflying
			<br>
			<input type="radio" name="jumpType" value="3">Solo Student
			<br>
			<input type="radio" name="jumpType" value="4">Student/AFF
			<br>
			<input type="radio" name="jumpType" value="5">Tandem
			<br>
			<input type="radio" name="jumpType" value="6">Tracking
			<br>
			<input type="radio" name="jumpType" value="7">Wingsuiting
			<br>
			<input type="radio" name="jumpType" value="8">High Pull
			<br>
			</td>
			</tr>
			<td>Group Name:  </td>
			<td><input type="text" name="GroupName" placeholder = "Enter Group Name"></td>
			</tr>
			<tr>
			<!-- We need to hash this -->
			<td colspan = "2"><input type="submit" value = "Add Me"></td>
			</tr>
		</table>
		</form>
		
	<footer>
			<a href = "HomePage.html">Home</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Login.html">Login</a>
			<a id = "space"> &nbsp  </a>
			<a href = "CurrentLoads.php">View Current Loads</a>
			<a id = "space"> &nbsp  </a>
			<a href = "ViewInformation.html">View Skydiver Information</a>
			<a id = "space"> &nbsp  </a>
			<a href = "AdminLogin.php">Administrator</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Rigging.html">Rigging</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Logout.php">Logout</a>
	</footer>
	</body>
</html>