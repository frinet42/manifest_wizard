<html>
 <head>
   <title>Add Skydiver Confirmation Page</title>
   <style> 
     body {font-family: Arial; color: black; } 
     h1 {background-color: blue; color: white; }
   </style>
 </head>

 <body>
   <center>
     <h1>Skydiver Added!</h1>
     <br />
     <br />

<!-- Note the use of <?php ?> to embed PHP commands 
     and $_POST['<parameter_name>'] to get POST parameters -->

     <?php 

     // open connection to the database on LOCALHOST with 
     // userid of 'root', password 'secret', and database 'csl'

     @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

     // Check if there were error and if so, report and exit

     if (mysqli_connect_errno()) 
     { 
       echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
       exit;
     }

		

     // sanitize the input from the form to eliminate possible SQL Injection

	if (isset($_POST['Admin'])) 
    { $Skydiver_Admin='Y'; }
     else 
    { $Skydiver_Admin ='N'; }
   
	$USPA_Num = stripslashes($_POST['USPANum']);
	$USPA_Num = htmlspecialchars($USPA_Num);
    $USPA_Num = $db->real_escape_string($USPA_Num);
	
	$Skydiver_FName = stripslashes($_POST['FName']);
	$Skydiver_FName = htmlspecialchars($Skydiver_FName);
    $Skydiver_FName = $db->real_escape_string($Skydiver_FName);

	$Skydiver_LName = stripslashes($_POST['LName']);
	$Skydiver_LName = htmlspecialchars($Skydiver_LName);
	$Skydiver_LName = $db->real_escape_string($Skydiver_LName );

	$Liscence_Num = stripslashes($_POST['licenseNum']);
	$Liscence_Num = htmlspecialchars($Liscence_Num);
	$Liscence_Num = $db->real_escape_string($Liscence_Num);

	$Skydiver_DateBeganSport = stripslashes($_POST['timeInSport']);
	$Skydiver_DateBeganSport = htmlspecialchars($Skydiver_DateBeganSport);
	$Skydiver_DateBeganSport = $db->real_escape_string($Skydiver_DateBeganSport);

	$Skydiver_NumOfJumps = stripslashes($_POST['jumps']);
	$Skydiver_NumOfJumps = htmlspecialchars($Skydiver_NumOfJumps);
	$Skydiver_NumOfJumps = $db->real_escape_string($Skydiver_NumOfJumps);

	$Skydiver_DOB = stripslashes($_POST['DOB']);
	$Skydiver_DOB= htmlspecialchars($Skydiver_DOB);
	$Skydiver_DOB = $db->real_escape_string($Skydiver_DOB);
	
	$Skydiver_ReserveRepackDate = stripslashes($_POST['repackDate']);
	$Skydiver_ReserveRepackDate = htmlspecialchars($Skydiver_ReserveRepackDate);
	$Skydiver_ReserveRepackDate = $db->real_escape_string($Skydiver_ReserveRepackDate);
	
	$Skydiver_DateOfLastJump = stripslashes($_POST['lastJump']);
	$Skydiver_DateOfLastJump = htmlspecialchars($Skydiver_DateOfLastJump);
	$Skydiver_DateOfLastJump = $db->real_escape_string($Skydiver_DateOfLastJump);
	
	$Skydiver_Type = stripslashes($_POST['type']);
	$Skydiver_Type = htmlspecialchars($Skydiver_Type);
	$Skydiver_Type = $db->real_escape_string($Skydiver_Type);
	
	$Skydiver_Password = stripslashes($_POST['Password']);
	$Skydiver_Password = htmlspecialchars($Skydiver_Password);
	$Skydiver_Password = $db->real_escape_string($Skydiver_Password);
	
	 // Server side data validation
	 
			$validUSPANum = "/^\d{6}$/";
	 		$validPassword = "/^\S+$/";
			$validPasswordShort = "/^\S{1,5}$/";
			$validPasswordLong = "/^\S{16,}$/";
			$validBlanketDate = "/^\d{4}\-\d{2}\-\d{2}$/";
			$validNameOrType = "/^[a-zA-z]+$/";
			$validLicense = "/^[ABCD]\-\d{1,}$/";
			$validJumpNum = "/^\d{1,}$/";
			
				if(!preg_match($validUSPANum,$USPA_Num)){
					echo("INSERT ERROR: Please enter your valid USPA number");
					header("location:Administrator.php");
				}
				if(!preg_match($validNameOrType,$Skydiver_FName)){
					echo("INSERT ERROR: Please enter the skydiver's first name");
					header("location:Administrator.php");
				}
				if(!preg_match($validNameOrType,$Skydiver_LName)){
					echo("INSERT ERROR: Please enter the skydiver's last name");
					header("location:Administrator.php");
				}
				if(!preg_match($validLicense,$Liscence_Num)){
					echo("INSERT ERROR: Please enter a valid license number (L-DDDD...)");
					header("location:Administrator.php");
				}
				if(!preg_match($validBlanketDate,$Skydiver_DateBeganSport)){
					echo("INSERT ERROR: Please enter a valid Time in Sport date (YYYY-MM-DD)");
					header("location:Administrator.php");
				}
				if(!preg_match($validJumpNum,$Skydiver_NumOfJumps)){
					echo("INSERT ERROR: Please enter a valid jump number");
					header("location:Administrator.php");
				}
				if(!preg_match($validBlanketDate,$Skydiver_DOB)){
					echo("INSERT ERROR: Please enter a valid DOB(YYYY-MM-DD)");
					header("location:Administrator.php");
				}
				if(!preg_match($validBlanketDate,$Skydiver_ReserveRepackDate)){
					echo("INSERT ERROR: Please enter a valid repack date (YYYY-MM-DD)");
					header("location:Administrator.php");
				}
				if(!preg_match($validBlanketDate,$Skydiver_DateOfLastJump)){
					echo("INSERT ERROR: Please enter a last jump date (YYYY-MM-DD)");
					header("location:Administrator.php");
				}
				if(!preg_match($validNameOrType,$Skydiver_Type)){
					echo("INSERT ERROR: Please enter a valid skydiver type");
					header("location:Administrator.php");
				}
				if(!preg_match($validPassword,$Skydiver_Password)){
					echo("INSERT ERROR: Please enter your password.");
					header("location:Administrator.php");
				}
				if(preg_match($validPasswordShort,$Skydiver_Password)){
					echo("INSERT ERROR: The password entered was too short.");
					header("location:Administrator.php");
				}
				if(preg_match($validPasswordLong,$Skydiver_Password)){
					echo("INSERT ERROR: The password entered was too long.");
					header("location:Administrator.php");
				}	
	
	$Skydiver_Password = md5($Skydiver_Password);
	
     // set up a prepared statement to insert the Skydiver
	 /*
	-- -----------------------------------------------------
	-- Table `Manifest_Wizard`.`Skydiver`
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS `Manifest_Wizard`.`Skydiver` ;

	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Skydiver` (
	`USPA_Num` INT NOT NULL,
	`Liscence_Num` INT NULL,
	`Skydiver_DateBeganSport` DATE NOT NULL,
	`Skydiver_NumOfJumps` INT NOT NULL,
	`Skydiver_FName` CHAR(45) NOT NULL,
	`Skydiver_LName` CHAR(45) NOT NULL,
	`Skydiver_DOB` DATE NOT NULL,
	`Skydiver_ReserveRepackDate` DATE NULL,
	`Skydiver_DateOfLastJump` DATE NULL,
	`Skydiver_Type` CHAR(1) NOT NULL,
	`Skydiver_Password` VARCHAR(8),
	'Skydiver_Admin' BOOLEAN,
	PRIMARY KEY (`USPA_Num`))
	ENGINE = InnoDB;

	*/

     $query = "INSERT INTO SKYDIVER (USPA_Num, Liscence_Num, Skydiver_DateBeganSport, Skydiver_NumOfJumps, Skydiver_FName, 
			 Skydiver_LName, Skydiver_DOB, Skydiver_ReserveRepackDate, Skydiver_DateOfLastJump, Skydiver_Type, Skydiver_Password,
			 Skydiver_Admin) 
	         VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";  // question marks are parameter locations

     $stmt = $db->prepare($query);  // creates the Prepared Statement

	 // binds the parameters of Prepared Statement to corresponding variables
	 // first argument, "sssiss", gives the parameter data types of 3 strings, an int, 2 strings
     $stmt->bind_param("ississssssss", $USPA_Num, $Liscence_Num, $Skydiver_DateBeganSport, $Skydiver_NumOfJumps, $Skydiver_FName, 
			 $Skydiver_LName, $Skydiver_DOB, $Skydiver_ReserveRepackDate, $Skydiver_DateOfLastJump, $Skydiver_Type, $Skydiver_Password,
			 $Skydiver_Admin);

     $stmt->execute();  // runs the Prepared Statement query

     echo $stmt->affected_rows.' records inserted.<br/><br/>';  // report results

     $stmt->close();  // deallocate the Prepared Statement
     $db->close();    // close the database connection
	 header("location:Administrator.php");
   ?>

     
   </center> 
 </body>
</html> 