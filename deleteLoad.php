<html>
 <head>
   <title>Delete Load Confirmation Page</title>
   <style> 
     body {font-family: Arial; color: black; } 
     h1 {background-color: blue; color: white; }
   </style>
 </head>

 <body>
   <center>
     <h1>Load Deleted!</h1>
     <br />
     <br />

<!-- Note the use of <?php ?> to embed PHP commands 
     and $_POST['<parameter_name>'] to get POST parameters -->

     <?php 

     // open connection to the database on LOCALHOST with 
     // userid of 'root', password 'secret', and database 'csl'

     @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

     // Check if there were error and if so, report and exit

     if (mysqli_connect_errno()) 
     { 
       echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
       exit;
     }


     // sanitize the input from the form to eliminate possible SQL Injection
	$Load_Num = stripslashes($_POST['loadNum']);
	$Load_Num = htmlspecialchars($Load_Num);
	$Load_Num = $db->real_escape_string($Load_Num);
	
	//Server side data validation
	$validLoadNumber = "/^\d{1,}$/";
				
	if(!preg_match($validLoadNumber,$Load_Num)){
					echo("LOAD ERROR: The selected load is not valid.");
					header("Location:Administrator.php");
				}
	
     // set up a prepared statement to insert the Skydiver
	 /*
	-- -----------------------------------------------------
	-- Table `Manifest_Wizard`.`Skydiver`
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS `Manifest_Wizard`.`Skydiver` ;

	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Skydiver` (
	`USPA_Num` INT NOT NULL,
	`Liscence_Num` INT NULL,
	`Skydiver_DateBeganSport` DATE NOT NULL,
	`Skydiver_NumOfJumps` INT NOT NULL,
	`Skydiver_FName` CHAR(45) NOT NULL,
	`Skydiver_LName` CHAR(45) NOT NULL,
	`Skydiver_DOB` DATE NOT NULL,
	`Skydiver_ReserveRepackDate` DATE NULL,
	`Skydiver_DateOfLastJump` DATE NULL,
	`Skydiver_Type` CHAR(1) NOT NULL,
	`Skydiver_Password` VARCHAR(8),
	'Skydiver_Admin' BOOLEAN,
	PRIMARY KEY (`USPA_Num`))
	ENGINE = InnoDB;

	*/
	$numQuery = $db->query("SELECT  Load_Capacity FROM _Load WHERE (Load_Num = {$Load_Num})");
	$numSpots = $numQuery->fetch_assoc();
	for ($j=1; $j <= $numSpots['Load_Capacity']; $j++) 
	{	
	$Skydiver = $db->query("DELETE FROM Load_Assignment WHERE (Load_Num = {$Load_Num})");
	}
	
	 $stmt = $db->prepare("DELETE FROM _LOAD WHERE (Load_Num = ?)");

     $stmt->bind_param("i", $Load_Num);

     $stmt->execute();
	 var_dump($stmt);
	 echo 'Load deleted.<br/><br/>';  // report results
     $db->close();    // close the database connection
	 header("location:Administrator.php");
   ?>

<!-- Give a link back to the main page -->

    

   </center> 
 </body>
</html> 