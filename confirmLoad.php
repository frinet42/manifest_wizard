<html>
 <head>
   <title>Add Load Confirmation Page</title>
   <style> 
     body {font-family: Arial; color: black; } 
     h1 {background-color: blue; color: white; }
   </style>
 </head>

 <body>
   <center>
     <h1>Load Added!</h1>
     <br />
     <br />

<!-- Note the use of <?php ?> to embed PHP commands 
     and $_POST['<parameter_name>'] to get POST parameters -->

     <?php 
	 date_default_timezone_set('America/Denver');
     // open connection to the database on LOCALHOST with 
     // userid of 'root', password 'secret', and database 'csl'

     @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

     // Check if there were error and if so, report and exit

     if (mysqli_connect_errno()) 
     { 
       echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
       exit;
     }


     // sanitize the input from the form to eliminate possible SQL Injection

	if (isset($_POST['Admin'])) 
    { $Skydiver_Admin='Y'; }
     else 
    { $Skydiver_Admin ='N'; }
   
	$Load_Num = stripslashes($_POST['loadNum']);
	$Load_Num = htmlspecialchars($Load_Num);
    $Load_Num = $db->real_escape_string($Load_Num);
	
	//Server side data validation
	$validLoadNumber = "/^\d{1,}$/";
	
	if(!preg_match($validLoadNumber,$Load_Num)){
					echo("LOAD ERROR: The selected load is not valid.");
					header("Location:Administrator.php");
				}
     // set up a prepared statement to insert the Skydiver
	 /*
	-- -----------------------------------------------------
	-- Table `Manifest_Wizard`.`Skydiver`
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS `Manifest_Wizard`.`Skydiver` ;

	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Skydiver` (
	`USPA_Num` INT NOT NULL,
	`Liscence_Num` INT NULL,
	`Skydiver_DateBeganSport` DATE NOT NULL,
	`Skydiver_NumOfJumps` INT NOT NULL,
	`Skydiver_FName` CHAR(45) NOT NULL,
	`Skydiver_LName` CHAR(45) NOT NULL,
	`Skydiver_DOB` DATE NOT NULL,
	`Skydiver_ReserveRepackDate` DATE NULL,
	`Skydiver_DateOfLastJump` DATE NULL,
	`Skydiver_Type` CHAR(1) NOT NULL,
	`Skydiver_Password` VARCHAR(8),
	'Skydiver_Admin' BOOLEAN,
	PRIMARY KEY (`USPA_Num`))
	ENGINE = InnoDB;

	*/

     $getJumpers = "SELECT * FROM Load_Assignment WHERE (Load_Num = {$Load_Num})";  

     $Skydivers = $db->query($getJumpers);
		$today = getdate();
		while ($r = $Skydivers->fetch_assoc())
		{	
			$getJumps = "SELECT Skydiver_NumOfJumps FROM Skydiver WHERE (USPA_Num = {$r['USPA_Num']})";  
			$Jumps = $db->query($getJumps);
			$j = $Jumps->fetch_assoc();
			$j['Skydiver_NumOfJumps'] = $j['Skydiver_NumOfJumps']+1;
			$updateJumpNum = $db->query("UPDATE SKYDIVER SET Skydiver_NumOfJumps ='{$j['Skydiver_NumOfJumps']}' WHERE (USPA_Num = {$r['USPA_Num']})");
			$Date = date($today['year']).':'.date($today['mon']).':'.date($today['mday']);
			$updateJumpDate = $db->query("UPDATE SKYDIVER SET Skydiver_DateOfLastJump ='{$Date}' WHERE (USPA_Num = {$r['USPA_Num']})");
		}
	 // binds the parameters of Prepared Statement to corresponding variables
	 // first argument, "sssiss", gives the parameter data types of 3 strings, an int, 2 strings
	 $DeleteConfirmedLoad = $db->query("DELETE FROM Load_Assignment WHERE (Load_Num = {$Load_Num})");
	 $DeleteConfirmedLoad = $db->query("DELETE FROM _Load WHERE (Load_Num = {$Load_Num})");
     $db->close();    // close the database connection
	 header("location:Administrator.php");
   ?>

     
   </center> 
 </body>
</html> 