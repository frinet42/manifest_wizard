<?php
  // checklogin.php - 3/24/2011 - Steve Hadfield 
  // Shows how to check a login username and password 

  session_start();  // link to session info

  // check if username and password were sent
  if ( (isset($_POST['Username'])) && (isset($_POST['Password'])) )
  {     // authentication check

    // open connection to the database on LOCALHOST with 
    // userid of 'root', password of 'secret', and database 'csl'

    @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

	
	$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
    // Check if there were error and if so, report and exit

    if (mysqli_connect_errno()) 
    { 
      echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
      exit;
    }

    // sanitize the input from the form to eliminate possible SQL Injection

	/*
	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Skydiver` (
	`USPA_Num` INT NOT NULL,
	`Liscence_Num` INT NULL,
	`Skydiver_DateBeganSport` DATE NOT NULL,
	`Skydiver_NumOfJumps` INT NOT NULL,
	`Skydiver_FName` CHAR(45) NOT NULL,
	`Skydiver_LName` CHAR(45) NOT NULL,
	`Skydiver_DOB` DATE NOT NULL,
	`Skydiver_ReserveRepackDate` DATE NULL,
	`Skydiver_DateOfLastJump` DATE NULL,
	`Skydiver_Type` CHAR(1) NOT NULL,
	`Skydiver_Password` VARCHAR(8),
	PRIMARY KEY (`USPA_Num`))
	ENGINE = InnoDB;


	*/
    $USPA_Num = stripslashes($_POST['Username']);
    $USPA_Num = $db->real_escape_string($USPA_Num);

    $Skydiver_Password = stripslashes($_POST['Password']);
    $Skydiver_Password = $db->real_escape_string($Skydiver_Password);

	//Server Side Data Validation
	$validUSPANum = "/^\d{6}$/";
	$validPassword = "/^\S+$/";
	$validPasswordShort = "/^\S{1,5}$/";
	$validPasswordLong = "/^\S{16,}$/";
			
								if(!preg_match($validUSPANum,$USPA_Num)){
					echo("INSERT ERROR: Please enter your valid USPA number");
					header("location:AdminLogin.php");
				}
								if(!preg_match($validPassword,$Skydiver_Password)){
					echo("INSERT ERROR: Please enter your password.");
					header("location:AdminLogin.php");
				}
				if(preg_match($validPasswordShort,$Skydiver_Password)){
					echo("INSERT ERROR: The password entered was too short.");
					header("location:AdminLogin.php");
				}
				if(preg_match($validPasswordLong,$Skydiver_Password)){
					echo("INSERT ERROR: The password entered was too long.");
					header("location:AdminLogin.php");
				}
    // encrypt the password with MD5

   $Skydiver_Password = md5($Skydiver_Password); 

    // check that username / password pair exists
	

	$Administrator = "SELECT Skydiver_Admin FROM SKYDIVER WHERE(USPA_Num = ?) AND (Skydiver_Password = ?)";
	
	$Skydiver_Admin = $db->query($Administrator);
	
	// run the SQL query to retrieve the service partner info
	$Names = "SELECT Skydiver_FName, Skydiver_LName FROM SKYDIVER WHERE (USPA_Num = {$USPA_Num})";

    $results = $db->query($Names);

	// determine how many rows were returned

    $num_results = $results->num_rows;

    // loop through each row building the table rows and data columns

    for ($i=0; $i < $num_results; $i++) 
    {
    $r= $results->fetch_assoc();
	$Name = $r['Skydiver_FName'].$r['Skydiver_LName'];
    print $r['Skydiver_FName'].$r['Skydiver_LName'];
    }

    $checkQuery = "SELECT * FROM SKYDIVER WHERE (USPA_Num = ?) AND (Skydiver_Password = ?) AND (Skydiver_Admin = 'Y')";

    $checkStmt = $db->prepare($checkQuery);

    $checkStmt->bind_param("ss", $USPA_Num, $Skydiver_Password);
	
    $checkStmt->execute();

    $checkStmt->store_result();

    // check for SQL error or if username/password pair does not exist

	if ( ($checkStmt->errno <> 0) || ($checkStmt->num_rows == 0) )
	{
      $checkStmt->close();
	  session_destroy();
	 //$message = "You are not authorized to view the administrator page, contact the DZO";
	 //echo "<script type='text/javascript'>alert('$message');</script>";
	 //echo "<a href='AdminLogin.html'>Click Here</a> to return to the Administrator login page.";
	 header("location:AdminLogin.html");
	exit;
	  exit;
	}else{
    // login was successful

		$checkStmt->close();

		// set session variable for user name
		$_SESSION['Username'] = $_POST['Username'];
		$_SESSION['Admin'] = 'Y';
		$_SESSION['Name'] = $r['Skydiver_FName'].' '.$r['Skydiver_LName'];
		echo '<pre>';
	var_dump($_SESSION);
	echo '</pre>';
		header("location: Administrator.php"); 
		exit;
	}
  }else  // username and/or password were not sent
  {
    header("location:AdminLogin.html");
    exit;
  }

?>