<?PHP
 session_start();
 if ( (!isset($_SESSION['Username']) || $_SESSION['Admin'] == "N" || !isset($_SESSION['Admin'])) )
  {
	header("location:AdminLogin.html");
	exit;
  }

?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<!-- href="appointment.css" -->
	<head>
		<link type="text/css" rel="stylesheet" href= "Administrator.css" ></link> 
		<title>Administrator</title>
		<script type="text/javascript">
			var validUSPANum = /^\d{6}$/;
			var validPassword = /^\S+$/;
			var validPasswordShort = /^\S{1,5}$/;
			var validPasswordLong = /^\S{16,}$/;
			var validBlanketDate = /^\d{4}\-\d{2}\-\d{2}$/;
			var validNameOrType = /^[a-zA-z]+$/;
			var validLicense = /^[ABCD]\-\d{1,}$/;
			var validJumpNum = /^\d{1,}$/;
			
			var validLoadNumber = /^\d{1,}$/;
			var validTime = /^\d{2}\:\d{2}$/;
			
			function addSkydiverFormat()
			{
				var format = new Boolean(true);
				
				var Username = document.forms["addSkydiver"]["USPANum"].value;
				var FName = document.forms["addSkydiver"]["FName"].value;
				var LName = document.forms["addSkydiver"]["LName"].value;
				var licenseNum = document.forms["addSkydiver"]["licenseNum"].value;
				var timeInSport = document.forms["addSkydiver"]["timeInSport"].value;
				var jumps = document.forms["addSkydiver"]["jumps"].value;
				var DOB = document.forms["addSkydiver"]["DOB"].value;
				var repackDate = document.forms["addSkydiver"]["repackDate"].value;
				var lastJump = document.forms["addSkydiver"]["lastJump"].value;
				var type = document.forms["addSkydiver"]["type"].value;
				var Password = document.forms["addSkydiver"]["Password"].value;
				
				if(!validUSPANum.test(Username)){
					format = false;
					alert("INSERT ERROR: Please enter your valid USPA number");
				}
				if(!validNameOrType.test(FName)){
					format = false;
					alert("INSERT ERROR: Please enter the skydiver's first name");
				}
				if(!validNameOrType.test(LName)){
					format = false;
					alert("INSERT ERROR: Please enter the skydiver's last name");
				}
				if(!validLicense.test(licenseNum)){
					format = false;
					alert("INSERT ERROR: Please enter a valid license number (L-DDDD...)");
				}
				if(!validBlanketDate.test(timeInSport)){
					format = false;
					alert("INSERT ERROR: Please enter a valid Time in Sport date (YYYY-MM-DD)");
				}
				if(!validJumpNum.test(jumps)){
					format = false;
					alert("INSERT ERROR: Please enter a valid jump number");
				}
				if(!validBlanketDate.test(DOB)){
					format = false;
					alert("INSERT ERROR: Please enter a valid DOB(YYYY-MM-DD)");
				}
				if(!validBlanketDate.test(repackDate)){
					format = false;
					alert("INSERT ERROR: Please enter a valid repack date (YYYY-MM-DD)");
				}
				if(!validBlanketDate.test(lastJump)){
					format = false;
					alert("INSERT ERROR: Please enter a last jump date (YYYY-MM-DD)");
				}
				if(!validNameOrType.test(LName)){
					format = false;
					alert("INSERT ERROR: Please enter a valid skydiver type");
				}
				if(!validPassword.test(Password)){
					format = false;
					alert("INSERT ERROR: Please enter your password.");
				}
				if(validPasswordShort.test(Password)){
					format = false;
					alert("INSERT ERROR: The password entered was too short.");
				}
				if(validPasswordLong.test(Password)){
					format = false;
					alert("INSERT ERROR: The password entered was too long.");
				}
				return format;
			}
			
			function addLoad(){
				var format = new Boolean(true);
				
				var numSpots = document.forms["AddLoad"]["numSpots"].value;
				var time = document.forms["AddLoad"]["time"].value;
				if(!validTime.test(time)){
					format = false;
					alert("LOAD ERROR: Please enter a valid time (HH:MM).");
				}
				if(!validLoadNumber.test(numSpots)){
					format = false;
					alert("LOAD ERROR: The number of spots needs to be greater than zero.");
				}
				return format;
			}
			
			function deleteLoad(){
				var format = new Boolean(true);
				
				var loadNum = document.forms["DeleteLoad"]["loadNum"].value;
				
				if(!validLoadNumber.test(loadNum)){
					format = false;
					alert("LOAD ERROR: The selected load is not valid.");
				}
				
				return format;
			}
			
			function confirmLoad(){
				var format = new Boolean(true);
				
				var loadNum = document.forms["ConfirmLoad"]["loadNum"].value;
				
				if(!validLoadNumber.test(loadNum)){
					format = false;
					alert("LOAD ERROR: The selected load is not valid.");
				}
				
				return format;
			}
			
			
		
			<!--<input type="radio" name="updateInfo" value="Skydiver_NumOfJumps">Number of Jumps
			<!--<br>
			<!--<input type="radio" name="updateInfo" value="Skydiver_Password">Password
			<!--<br>
			<!--<input type="radio" name="updateInfo" value="Skydiver_Admin">Give Admin Privileges (No new info required)
			-->
			function updateSkydiverFormat()
			{
				var format = new Boolean(true);
				
				var Username = document.forms["updateSkydiverInformation"]["USPANum"].value;
				var whatToUpdate = document.forms["updateSkydiverInformation"]["updateInfo"].value;
				var info = document.forms["updateSkydiverInformation"]["newInfo"].value;
				
				if(!validUSPANum.test(Username)){
					format = false;
					alert("INSERT ERROR: Please enter your valid USPA number");
				}
				if(whatToUpdate == "Skydiver_FName" || whatToUpdate == "Skydiver_LName" ){
					if(!validNameOrType.test(info)){
					format = false;
					alert("INSERT ERROR: Please enter a valid name");
				}
				}
				
				if(whatToUpdate == "Liscence_Num"){
					if(!validLicense.test(info)){
					format = false;
					alert("INSERT ERROR: Please enter a valid license number (L-DDDD...)");
				}
				}
				
				if(whatToUpdate == "Skydiver_DateBeganSport" || whatToUpdate == "Skydiver_DOB" || whatToUpdate =="Skydiver_ReserveRepackDate"
				|| whatToUpdate == "Skydiver_DateOfLastJump"){
					if(!validBlanketDate.test(info)){
					format = false;
					alert("INSERT ERROR: Please enter a valid date (YYYY-MM-DD)");
				}
				}
				
				if(whatToUpdate == "Skydiver_Type"){
					if(!validNameOrType.test(info)){
					format = false;
					alert("INSERT ERROR: Please enter a valid skydiver type.");
				}
				}
				if(whatToUpdate == "Skydiver_NumOfJumps"){
				if(!validJumpNum.test(info)){
					format = false;
					alert("INSERT ERROR: Please enter a valid jump number");
				}
				}
				if(whatToUpdate == "Skydiver_Password"){
				if(!validPassword.test(info)){
					format = false;
					alert("INSERT ERROR: Please enter your password.");
				}
				if(validPasswordShort.test(info)){
					format = false;
					alert("INSERT ERROR: The password entered was too short.");
				}
				if(validPasswordLong.test(info)){
					format = false;
					alert("INSERT ERROR: The password entered was too long.");
				}
				}
				return format;
			}
			
			function printLoad(){
				var format = new Boolean(true);
				
				var loadNum = document.forms["exitOrder"]["loadNum"].value;
				
				if(!validLoadNumber.test(loadNum)){
					format = false;
					alert("LOAD ERROR: The selected load is not valid.");
				}
				
				return format;
			}
		</script>
	</head>
	<body link="white">
		<div>
		<form name = "addSkydiver" action="AddSkydiver.php" onsubmit="return addSkydiverFormat();" method = "POST">
		<table>
			<caption>Add a skydiver</caption>
			<tr>
				<td>USPA Number:</td>
				<td><input type="text" name="USPANum" placeholder="6 digit USPA number"></td>
			</tr>
			<tr>
				<td>First Name:</td>
				<td><input type="text" name="FName" placeholder="First name"></td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td><input type="text" name="LName" placeholder="Last name"></td>
			</tr>
			<tr>
				<td>License Number:</td>
				<td><input type="text" name="licenseNum" placeholder="License #"></td>
			</tr>
			<tr>
				<td>Time in Sport:</td>
				<td><input type="text" name="timeInSport" placeholder="First jump date"></td>
			</tr>
			<tr>
				<td>Number of Jumps:</td>
				<td><input type="text" name="jumps" placeholder="# of Jumps"></td>
			</tr>
			<tr>
				<td>DOB:</td>
				<td><input type="text" name="DOB" placeholder="DOB"></td>
			</tr>
			<tr>
				<td>Reserve Repack Date:</td>
				<td><input type="text" name="repackDate" placeholder="Repack Date"></td>
			</tr>
			<tr>
				<td>Last Jump:</td>
				<td><input type="text" name="lastJump" placeholder="Date of last jump"></td>
			</tr>
			<tr>
				<td>Type:</td>
				<td><input type="text" name="type" placeholder="Student/Instructor/Fun"></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="text" name="Password" placeholder="1-15 character password "></td>
			</tr>
			<tr>
			<td>Administrator?  </td>
			<td><input type="checkbox" name="Admin" value="Yes" align="left">Check if Administrator</input></td>
			<!-- We need to hash this -->
			</tr>
			<tr>
			<td colspan = "2"><input type="submit" value = "Add Jumper"></td>
			</tr>
		</table>
		</form>
		</div>
		<div>
		<form name = "AddLoad" action = "addLoad.php" onsubmit="return addLoad();" method = "POST">
		<table>
			<caption>Load Options</caption>
			<tr>
				<td>Load Time:</td>
				<td><input type="text" name="time" placeholder="Time (24 hour)"></td>
			</tr>
			<tr>
				<td>Available Spots</td>
				<td><input type="text" name="numSpots" placeholder="How many spots?"></td>
			</tr>
			<tr>
				<td colspan = "2"><input type="submit" value = "Add New Load"></td>
			</tr>
			</form>
			<form name = "DeleteLoad" action = "deleteLoad.php" onsubmit="return deleteLoad();" method = "POST">
			<tr>
			<td>Delete Load: </td>
			<td><select name = "loadNum">
			<?php
			
			@ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

	
			$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
			// Check if there were error and if so, report and exit

			if (mysqli_connect_errno()) 
			{ 
			echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
			exit;
			}
				$numLoads = "SELECT  COUNT(*) FROM _Load";
				// run the SQL query to retrieve the service partner info
				$totalLoads = $db->query($numLoads);
				$numLoads = $totalLoads->fetch_assoc();
				// determine how many rows were returned
				$num_results = $numLoads['COUNT(*)'];
				 for ($i=1; $i <= $num_results; $i++) 
				{
				$loadExistQuery = $db->query("SELECT COUNT(*) FROM _Load WHERE (Load_Num = {$i})");
				$rows = $loadExistQuery->fetch_assoc();
				var_dump($rows);
				if($rows['COUNT(*)'] == 1){
					print '<option value="'.$i.'">'.$i.'</option>';
				}else{
					$num_results = $num_results+1;
				}
				}
			?>
			</select></td></tr>
			<tr>
				<td colspan = "2"><input type="submit" value = "Delete Load" ></td>
			</tr>
			</form>
			<form name = "ConfirmLoad" action = "confirmLoad.php" onsubmit="return confirmLoad();" method = "POST">
			<tr>
			<td>Confirm Load: </td>
			<td><select name = "loadNum">
			<?php
			
			@ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

	
			$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
			// Check if there were error and if so, report and exit

			if (mysqli_connect_errno()) 
			{ 
			echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
			exit;
			}
				$numLoads = "SELECT  COUNT(*) FROM _Load";
				// run the SQL query to retrieve the service partner info
				$totalLoads = $db->query($numLoads);
				$numLoads = $totalLoads->fetch_assoc();
				// determine how many rows were returned
				$num_results = $numLoads['COUNT(*)'];
				 for ($i=1; $i <= $num_results; $i++) 
				{
				$loadExistQuery = $db->query("SELECT COUNT(*) FROM _Load WHERE (Load_Num = {$i})");
				$rows = $loadExistQuery->fetch_assoc();
				var_dump($rows);
				if($rows['COUNT(*)'] == 1){
					print '<option value="'.$i.'">'.$i.'</option>';
				}else{
					$num_results = $num_results+1;
				}
				}
			?>
			</select></td>
			</tr>
			<tr>
				<td colspan = "2"><input type="submit" value = "Confirm Load"></td>
			</tr>
		</table>
		</form>
		</div>
		<div>
		<form name = "updateSkydiverInformation" action = "updateSkydiver.php" onsubmit="return updateSkydiverFormat();" method = "POST">
		<table>
			<caption>Update Skydiver Information</caption>
			<tr>
				<td>USPA Number:</td>
				<td><input type="text" name="USPANum" placeholder="USPA Number"></td>
			</tr>
			<tr>
			<td>What to update:</td>
			<td><input type="radio" name="updateInfo" value="Skydiver_FName" checked>First Name
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_LName">Last Name
			<br>
			<input type="radio" name="updateInfo" value="Liscence_Num">License Number
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_DateBeganSport">Time in Sport
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_NumOfJumps">Number of Jumps
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_DOB">DOB
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_ReserveRepackDate">Reserve Repack Date
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_DateOfLastJump">Last Jump
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_Type">Type
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_Password">Password
			<br>
			<input type="radio" name="updateInfo" value="Skydiver_Admin">Give Admin Privileges (No new info required)
			<br>
			</td>
			</tr>
			<tr>
				<td>Updated Information:</td>
				<td><input type="text" name="newInfo" placeholder="What is the new info?"></td>
			</tr>
			<!-- We need to hash this -->
			<td colspan = "2"><input type="submit" value = "Update Jumper"></td>
			</tr>
		</table>
		</form>
		</div>
		<div>
		<form name = "riggingRequest" action = "riggingRequests.php" method = "POST">
		<table>
			<caption>View All Rigging Requests</caption>
			<tr>
				<td colspan = "2"><input type="submit" value = "View Rigging Requests"></td>
			</tr>
		</table>
		</form>
		</div>
		<div>
		<form name = "exitOrder" action = "loadPrinter.php" onsubmit="return printLoad()" method = "POST">
		<table>
			<caption>Print Exit Order</caption>
			<tr>
			<td>Load Number: </td>
			<td><select name = "loadNum">
			<?php
			
			@ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

	
			$link = mysqli_connect('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');
			// Check if there were error and if so, report and exit

			if (mysqli_connect_errno()) 
			{ 
			echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
			exit;
			}
				$numLoads = "SELECT  COUNT(*) FROM _Load";
				// run the SQL query to retrieve the service partner info
				$totalLoads = $db->query($numLoads);
				$numLoads = $totalLoads->fetch_assoc();
				// determine how many rows were returned
				$num_results = $numLoads['COUNT(*)'];
				 for ($i=1; $i <= $num_results; $i++) 
				{
				$loadExistQuery = $db->query("SELECT COUNT(*) FROM _Load WHERE (Load_Num = {$i})");
				$rows = $loadExistQuery->fetch_assoc();
				var_dump($rows);
				if($rows['COUNT(*)'] == 1){
					print '<option value="'.$i.'">'.$i.'</option>';
				}else{
					$num_results = $num_results+1;
				}
				}
			?>
			</select></td>
			</tr>
			<tr>
				<td colspan = "2"><input type="submit" value = "Print Load"></td>
			</tr>
		</table>
		</form>
		</div>
	<footer>
			<a href = "HomePage.html">Home</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Login.html">Login</a>
			<a id = "space"> &nbsp  </a>
			<a href = "CurrentLoads.php">View Current Loads</a>
			<a id = "space"> &nbsp  </a>
			<a href = "ViewInformation.html">View Skydiver Information</a>
			<a id = "space"> &nbsp  </a>
			<a href = "AdminLogin.php">Administrator</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Rigging.html">Rigging</a>
			<a id = "space"> &nbsp  </a>
			<a href = "Logout.php">Logout</a>
	</footer>
	</body>
</html>