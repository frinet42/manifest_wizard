<html>
 <head>
   <title>Add Load Confirmation Page</title>
   <style> 
     body {font-family: Arial; color: black; } 
     h1 {background-color: blue; color: white; }
   </style>
 </head>

 <body>
   <center>
     <h1>Load Added!</h1>
     <br />
     <br />

<!-- Note the use of <?php ?> to embed PHP commands 
     and $_POST['<parameter_name>'] to get POST parameters -->

     <?php 

     // open connection to the database on LOCALHOST with 
     // userid of 'root', password 'secret', and database 'csl'

     @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

     // Check if there were error and if so, report and exit

     if (mysqli_connect_errno()) 
     { 
       echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
       exit;
     }
	 
	 //PHP Server Side Input Validation
	 
	 $validLoadNumber = "/^\d{1,}$/";
	 $validTime = "/^\d{2}\:\d{2}$/";
				
	 $numSpots = $_POST['numSpots'];
	 $time = $_POST['time'];
	 
				if(!preg_match($validLoadNumber,$numSpots)){
					echo("LOAD ERROR: The number of spots needs to be greater than zero.");
					header("location:Administrator.php");
				}
				if(!preg_match($validTime,$time)){
					echo("LOAD ERROR: Please enter a valid time (HH:MM).");
					header("location:Administrator.php");
				}

     // sanitize the input from the form to eliminate possible SQL Injection
 	
	$Load_Time = stripslashes($_POST['time']);
	$Load_Time = htmlspecialchars($Load_Time);
    $Load_Time = $db->real_escape_string($Load_Time);

	$Load_Capacity = stripslashes($_POST['numSpots']);
	$Load_Capacity = htmlspecialchars($Load_Capacity);
	$Load_Capacity = $db->real_escape_string($Load_Capacity);

	
     // set up a prepared statement to insert the Skydiver
	 /*
	-- -----------------------------------------------------
	-- Table `Manifest_Wizard`.`Skydiver`
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS `Manifest_Wizard`.`Skydiver` ;

	CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Skydiver` (
	`USPA_Num` INT NOT NULL,
	`Liscence_Num` INT NULL,
	`Skydiver_DateBeganSport` DATE NOT NULL,
	`Skydiver_NumOfJumps` INT NOT NULL,
	`Skydiver_FName` CHAR(45) NOT NULL,
	`Skydiver_LName` CHAR(45) NOT NULL,
	`Skydiver_DOB` DATE NOT NULL,
	`Skydiver_ReserveRepackDate` DATE NULL,
	`Skydiver_DateOfLastJump` DATE NULL,
	`Skydiver_Type` CHAR(1) NOT NULL,
	`Skydiver_Password` VARCHAR(8),
	'Skydiver_Admin' BOOLEAN,
	PRIMARY KEY (`USPA_Num`))
	ENGINE = InnoDB;

	*/

     $query = "INSERT INTO _LOAD (Load_Time, Load_Capacity) VALUE (?,?)";  // question marks are parameter locations

     $stmt = $db->prepare($query);  // creates the Prepared Statement

	 // binds the parameters of Prepared Statement to corresponding variables
	 // first argument, "sssiss", gives the parameter data types of 3 strings, an int, 2 strings
     $stmt->bind_param("si", $Load_Time, $Load_Capacity);

     $stmt->execute();  // runs the Prepared Statement query

     echo $stmt->affected_rows.' records inserted.<br/><br/>';  // report results

     $stmt->close();  // deallocate the Prepared Statement
     $db->close();    // close the database connection
	 header("location:Administrator.php");
   ?>

<!-- Give a link back to the main page -->

   </center> 
 </body>
</html> 