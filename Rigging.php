<html>
 <head>
   <title>Add Rigging Request</title>
   <style> 
     body {font-family: Arial; color: black; } 
     h1 {background-color: blue; color: white; }
   </style>
 </head>

 <body>
   <center>
     <h1>Request Added!</h1>
     <br />
     <br />

<!-- Note the use of <?php ?> to embed PHP commands 
     and $_POST['<parameter_name>'] to get POST parameters -->

     <?php 

     // open connection to the database on LOCALHOST with 
     // userid of 'root', password 'secret', and database 'csl'

     @ $db = new mysqli('LOCALHOST', 'root', 'secret', 'Manifest_Wizard');

     // Check if there were error and if so, report and exit

     if (mysqli_connect_errno()) 
     { 
       echo 'ERROR: Could not connect to database, error is '.mysqli_connect_error();
       exit;
     }
	session_start();
	 if(isset($_SESSION['Username'])){
     // sanitize the input from the form to eliminate possible SQL Injection
	 /*
CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Rigging` (
  `USPA_Num` INT NOT NULL,
  `Rigging_SerialNum` INT NOT NULL,
  `Rigging_WorkDone` VARCHAR(45) NOT NULL,
  `Rigging_Container` VARCHAR(30),
  `Rigging_Main` VARCHAR(30),
  `Rigging_Reserve` VARCHAR(30),
  `Rigging_AAD` VARCHAR(30),
  PRIMARY KEY (`USPA_Num`, `Rigging_SerialNum`),
  CONSTRAINT `Rigging_USPA_Num`
    FOREIGN KEY (`USPA_Num`)
    REFERENCES `Manifest_Wizard`.`Skydiver` (`USPA_Num`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;
  */
	$USPA_Num = $_SESSION['Username'];
    $USPA_Num = $db->real_escape_string($USPA_Num);

	$Rigging_WorkDone = stripslashes($_POST['requestType']);
	$Rigging_WorkDone = htmlspecialchars($Rigging_WorkDone);
	$Rigging_WorkDone = $db->real_escape_string($Rigging_WorkDone);
	
	$Rigging_Container = stripslashes($_POST['SerialNum']);
	$Rigging_Container = htmlspecialchars($Rigging_Container);
	$Rigging_Container = $db->real_escape_string($Rigging_Container);

	$Rigging_Main = stripslashes($_POST['MainType']);
	$Rigging_Main = htmlspecialchars($Rigging_Main);
	$Rigging_Main = $db->real_escape_string($Rigging_Main);

	$Rigging_Reserve = stripslashes($_POST['ReserveType']);
	$Rigging_Reserve = htmlspecialchars($Rigging_Reserve);
	$Rigging_Reserve = $db->real_escape_string($Rigging_Reserve);

	$Rigging_AAD = stripslashes($_POST['AADType']);
	$Rigging_AAD = htmlspecialchars($Rigging_AAD);
	$Rigging_AAD = $db->real_escape_string($Rigging_AAD);
	
	//Server Side data validation
	$validUSPANum = "/^\d{6}$/";
	$validFormat = "/^\S+$/";
	
	if(!preg_match($validUSPANum,$USPA_Num)){
					echo("INSERT ERROR: Please enter your valid USPA number");
					header("location:Rigging.html");
				}
				
	if(!preg_match($validFormat,$Rigging_Container)){
					echo("ERROR: Please enter a valid Serial Number");
					header("location:Rigging.html");
				}
	if(!preg_match($validFormat,$Rigging_Main)){
					echo("ERROR: Please enter a valid main");
					header("location:Rigging.html");
				}
	if(!preg_match($validFormat,$Rigging_Reserve)){
					echo("ERROR: Please enter a valid reserve");
					header("location:Rigging.html");
				}
	if(!preg_match($validFormat,$Rigging_AAD)){
					echo("ERROR: Please enter a valid AAD");
					header("location:Rigging.html");
				}
				
	if(!preg_match($validFormat,$Rigging_WorkDone)){
					echo("ERROR: Please select a rigging type");
					header("location:Rigging.html");
				}
     // set up a prepared statement to insert the Skydiver
	 /*
   CREATE TABLE IF NOT EXISTS `Manifest_Wizard`.`Rigging` (
  `Rigging_SerialNum` INT NOT NULL,
  `USPA_Num` INT NOT NULL,
  `Rigging_WorkDone` VARCHAR(45) NOT NULL,
  `Rigging_Container` VARCHAR(30),
  `Rigging_Main` VARCHAR(30),
  `Rigging_Reserve` VARCHAR(30),
  `Rigging_AAD` VARCHAR(30),
  PRIMARY KEY (`USPA_Num`, `Rigging_SerialNum`),
  CONSTRAINT `Rigging_USPA_Num`
    FOREIGN KEY (`USPA_Num`)
    REFERENCES `Manifest_Wizard`.`Skydiver` (`USPA_Num`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

	*/

     $query = "INSERT INTO RIGGING (USPA_Num, Rigging_WorkDone, Rigging_Container, Rigging_Main, Rigging_Reserve, Rigging_AAD ) 
	         VALUES ( ?, ?, ?, ?, ?, ?)";  // question marks are parameter locations

     $stmt = $db->prepare($query);  // creates the Prepared Statement

	 // binds the parameters of Prepared Statement to corresponding variables
	 // first argument, "sssiss", gives the parameter data types of 3 strings, an int, 2 strings
     $stmt->bind_param("isssss", $USPA_Num, $Rigging_WorkDone, $Rigging_Container, $Rigging_Main, $Rigging_Reserve, $Rigging_AAD);

     $stmt->execute();  // runs the Prepared Statement query

     echo $stmt->affected_rows.' records inserted.<br/><br/>';  // report results
	 
	 echo $_POST['requestType'];


     $stmt->close();  // deallocate the Prepared Statement
     $db->close();    // close the database connection
	 }else{
     $db->close();
	 header("location:Login.html");
	 exit;
	 }
	 header("location:Rigging.html");
   ?>

   </center> 
 </body>
</html> 